package de.thbingen.epro;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.thbingen.epro.config.DirectExchangeConfiguration;
import de.thbingen.epro.listener.Consumer;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {DirectExchangeConfiguration.class})
public class DirectExchangeTest {


	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private DirectExchange exchange;
	

	@Autowired
	protected RabbitAdmin rabbitAdmin;

	@Autowired
	private ConnectionFactory connectionFactory;

	
	@Test
	public void test() {

		Consumer consumer1 = createAndBindListener("routing-key-1", 1);
		Consumer consumer2 = createAndBindListener("routing-key-2", 2);

		rabbitTemplate.convertAndSend("routing-key-1", buildMessage("direct message body 1"));
		rabbitTemplate.convertAndSend("routing-key-2", buildMessage("direct message body 2"));
		rabbitTemplate.convertAndSend("routing-key-2", buildMessage("direct message body 3"));

		
		try {
			consumer1.getLatch().await(1, TimeUnit.SECONDS);
			consumer2.getLatch().await(2, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertThat(consumer1.getReceivedMessages().size(), is(1));
		assertThat(consumer1.getReceivedMessages().get(0).getBody(), is("message body 1".getBytes()));
		assertThat(consumer2.getReceivedMessages().size(), is(2));
		assertThat(consumer2.getReceivedMessages().get(0).getBody(), is("message body 2".getBytes()));
		assertThat(consumer2.getReceivedMessages().get(1).getBody(), is("message body 3".getBytes()));
	}
	
	public Message buildMessage(String messageBody) {
		return MessageBuilder.withBody(messageBody.getBytes()).build();
	}

	public Queue declareQueue(String routingKey) {
		Queue queue = new Queue(routingKey, true, false, false);
		rabbitAdmin.declareQueue(queue);
		rabbitAdmin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(routingKey));
		return queue;
	}
	
	public void sendMessage(String routingKey, String message) {
		rabbitTemplate.convertAndSend(routingKey, buildMessage(message));
	}
	
	public Consumer createAndBindListener(String routingKey, int expectedMessages) {
		Consumer listener = new Consumer(expectedMessages);
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
		container.setMessageListener(listener);
		container.setQueues(declareQueue(routingKey));
		container.start();
		return listener;
	}
}
