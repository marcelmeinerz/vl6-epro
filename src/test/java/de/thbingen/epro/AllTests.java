package de.thbingen.epro;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TopicExchangeTest.class, FanoutExchangeTest.class, DirectExchangeTest.class })
public class AllTests {

}
