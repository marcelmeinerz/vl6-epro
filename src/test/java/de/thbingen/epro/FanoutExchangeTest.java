package de.thbingen.epro;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.thbingen.epro.config.FanoutExchangeConfiguration;
import de.thbingen.epro.listener.Consumer;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {FanoutExchangeConfiguration.class})
public class FanoutExchangeTest {

    
    @Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	protected RabbitAdmin rabbitAdmin;

	@Autowired
	private ConnectionFactory connectionFactory;
    
    @Autowired
	private FanoutExchange exchange;
	
    
    @Test
    public void test() throws InterruptedException {
        byte[] messageBody = "fanout message".getBytes();
        Message message = MessageBuilder.withBody(messageBody).build();

        Consumer consumer1 = createAndBindListener("fanout-declareQueue-1",1);
        Consumer consumer2 = createAndBindListener("fanout-declareQueue-2",1);
        Consumer consumer3 = createAndBindListener("fanout-declareQueue-3",1);

        rabbitTemplate.convertAndSend(message);

        consumer1.getLatch().await(1, TimeUnit.SECONDS);
        consumer2.getLatch().await(1, TimeUnit.SECONDS);
        consumer3.getLatch().await(1, TimeUnit.SECONDS);

        assertThat(consumer1.getReceivedMessages().size(), is(1));
        assertThat(consumer2.getReceivedMessages().size(), is(1));
        assertThat(consumer3.getReceivedMessages().size(), is(1));

        assertThat(consumer1.getReceivedMessages().get(0).getBody(), is(messageBody));
        assertThat(consumer2.getReceivedMessages().get(0).getBody(), is(messageBody));
        assertThat(consumer3.getReceivedMessages().get(0).getBody(), is(messageBody));
    }

    public Queue declareQueue(String routingKey) {
		Queue queue = new Queue(routingKey, true, false, false);
		rabbitAdmin.declareQueue(queue);
		rabbitAdmin.declareBinding(BindingBuilder.bind(queue).to(exchange));
		return queue;
	}

	public Message buildMessage(String messageBody) {
		return MessageBuilder.withBody(messageBody.getBytes()).build();
	}

	public void sendMessage(String routingKey, String message) {
		rabbitTemplate.convertAndSend(routingKey, buildMessage(message));
	}
	
	public Consumer createAndBindListener(String routingKey, int expectedMessages) {
		Consumer listener = new Consumer(expectedMessages);
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
		container.setMessageListener(listener);
		container.setQueues(declareQueue(routingKey));
		container.start();
		return listener;
	}

}
