package de.thbingen.epro;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.thbingen.epro.config.TopicExchangeConfiguration;
import de.thbingen.epro.listener.Consumer;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {TopicExchangeConfiguration.class})
public class TopicExchangeTest {

	
	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	protected RabbitAdmin rabbitAdmin;

	@Autowired
	private ConnectionFactory connectionFactory;
	
	@Autowired
	private TopicExchange exchange;
	
	@Test
	public void test() {

		   Consumer consumer1 = createAndBindListener("topic.exchange.*", 3);
		   Consumer consumer2 = createAndBindListener("topic.exchange.a", 2);
		   Consumer consumer3 = createAndBindListener("topic.exchange.b", 1);

		   rabbitTemplate.convertAndSend("topic.exchange.a",  buildMessage("topic message 1"));
	        rabbitTemplate.convertAndSend("topic.exchange.a", buildMessage("topic message 2"));
	        rabbitTemplate.convertAndSend("topic.exchange.b", buildMessage("topic message 3"));

		
		try {
			consumer1.getLatch().await(3, TimeUnit.SECONDS);
			consumer2.getLatch().await(2, TimeUnit.SECONDS);
			consumer3.getLatch().await(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		assertThat(consumer1.getReceivedMessages().size(), is(3));
		assertThat(consumer2.getReceivedMessages().size(), is(2));
		assertThat(consumer3.getReceivedMessages().size(), is(1));


	}
	
	public Queue declareQueue(String routingKey) {
		Queue queue = new Queue(routingKey, true, false, false);
		rabbitAdmin.declareQueue(queue);
		rabbitAdmin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(routingKey));
		return queue;
	}
	public Message buildMessage(String messageBody) {
		return MessageBuilder.withBody(messageBody.getBytes()).build();
	}

	
	
	public void sendMessage(String routingKey, String message) {
		rabbitTemplate.convertAndSend(routingKey, buildMessage(message));
	}
	
	public Consumer createAndBindListener(String routingKey, int expectedMessages) {
		Consumer listener = new Consumer(expectedMessages);
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
		container.setMessageListener(listener);
		container.setQueues(declareQueue(routingKey));
		container.start();
		return listener;
	}
}
