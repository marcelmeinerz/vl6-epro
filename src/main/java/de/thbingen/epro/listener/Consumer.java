package de.thbingen.epro.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;


public class Consumer implements MessageListener {
	
	private static int ID = 0;
	
	private int id;
	
	private final List<Message> receivedMessages = new ArrayList<Message>();

    private final CountDownLatch latch;
    
    public Consumer() {
    	this(1);
    }
    
    public Consumer(int expectedMessages) {
        latch = new CountDownLatch(expectedMessages);
        id=ID++;
    }

    public void onMessage(Message message) {
    	System.out.println("Consumer "+ id +" received message: " + new String(message.getBody()));
    	this.receivedMessages.add(message);
        latch.countDown();
    }

	public CountDownLatch getLatch() {
		return latch;
	}

	public List<Message> getReceivedMessages() {
		return receivedMessages;
	}
    
    
}
