package de.thbingen.epro.config;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"de.thbingen.epro"})
public class TopicExchangeConfiguration extends RabbitConfiguration{
	private static final String TOPIC_EXCHANGE = "ExchangeTypesTests.TopicExchange";

    @Bean
    public TopicExchange directExchange(RabbitAdmin rabbitAdmin) {
        TopicExchange exchange = new TopicExchange(TOPIC_EXCHANGE);
        rabbitAdmin.declareExchange(exchange);
        return exchange;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setExchange(TOPIC_EXCHANGE);
        return template;
    }
}
