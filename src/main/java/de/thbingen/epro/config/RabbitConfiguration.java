package de.thbingen.epro.config;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"de.thbingen.epro", })
public class RabbitConfiguration {

	public final static String EXCHANGE_NAME = "simple.queue.name";

	public final static String ROUTING_KEY_1 = "topic.exchange.*";
	public final static String ROUTING_KEY_2 = "topic.exchange.a";
	public final static String ROUTING_KEY_3 = "topic.exchange.b*";
	public final static String ROUTING_KEY_4 = "demo.epro.spring";
	public final static String ROUTING_KEY_5 = "demo.epro.mongodb.*";
	public final static String ROUTING_KEY_6 = "demo.epro.maven.*";
	public final static String ROUTING_KEY_7 = "demo.epro.maven.#";

	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}

	@Bean
	public RabbitTemplate rabbitTemplate() {
		return new RabbitTemplate(connectionFactory());
	}

	@Bean
	public RabbitAdmin rabbitAdmin() {
		return new RabbitAdmin(connectionFactory());
	}
}
