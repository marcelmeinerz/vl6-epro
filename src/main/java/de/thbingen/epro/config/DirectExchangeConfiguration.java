package de.thbingen.epro.config;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"de.thbingen.epro"})
public class DirectExchangeConfiguration extends RabbitConfiguration{
	
	 private static final String DIRECT_EXCHANGE = "ExchangeTypesTests.DirectExchange";

     @Bean
     public DirectExchange directExchange(RabbitAdmin rabbitAdmin) {
         DirectExchange exchange = new DirectExchange(DIRECT_EXCHANGE);
         rabbitAdmin.declareExchange(exchange);
         return exchange;
     }

     @Bean
     public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
         RabbitTemplate template = new RabbitTemplate(connectionFactory);
         template.setExchange(DIRECT_EXCHANGE);
         return template;
     }

}
