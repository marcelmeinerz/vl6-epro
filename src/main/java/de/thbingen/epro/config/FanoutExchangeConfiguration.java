package de.thbingen.epro.config;


import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"de.thbingen.epro"})
public class FanoutExchangeConfiguration extends RabbitConfiguration{
	
	 private static final String FANOUT_EXCHANGE = "ExchangeTypesTests.FanoutExchange";

     @Autowired
     private RabbitAdmin rabbitAdmin;

     @Autowired
     private ConnectionFactory connectionFactory;

     @Bean
     public FanoutExchange fanoutExchange() {
         FanoutExchange exchange = new FanoutExchange(FANOUT_EXCHANGE);
         rabbitAdmin.declareExchange(exchange);
         return exchange;
     }

     @Bean
     public RabbitTemplate rabbitTemplate() {
         RabbitTemplate template = new RabbitTemplate(connectionFactory);
         template.setExchange(FANOUT_EXCHANGE);
         return template;
     }

     
}
